﻿using MySwoleMate.Models;
using MySwoleMate.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace MySwoleMate.BLL
{
    public class WorkoutBLL
    {
        // Instance of the Data Access Layer for Workout
        private WorkoutDal data;

        // constructor that accepts a connectionString from the Presentation Layer,
        // Use the connectionString to pass into a new instance of the Data Access Layer class WorkoutDal
        public WorkoutBLL(string connectionString)
        {
            data = new WorkoutDal(connectionString);
        }

        // Gets all Workouts in a List of WorkoutViewModel
        public List<WorkoutViewModel> GetAllWorkouts()
        {
            // return the List<WorkViewModel> from WorkoutDAL
            // Here you will need to use the methods in order to display the Excercises
            List<WorkoutViewModel> workouts = data.GetWorkouts();

            return workouts;
        }
        // Returns ViewModel of Workout by Id
        public WorkoutViewModel GetWorkoutById(int id)
        {
            return data.GetWorkoutById(id);
        }
        // Edits The Workout accepting a WorkoutViewModel
        public int EditWorkout(WorkoutViewModel edit)
        {
            return data.EditWorkout(edit);
        }
        // Add a new Workout
        public int AddWorkout(WorkoutViewModel add)
        {
            return data.AddWorkout(add);
        }
        // Deletes a Workout by the Id, Delete only needs the id of Workout
        public int DeleteWorkout(int id)
        {
            return data.DeleteWorkout(id);
        }

        //You can create the private methods for DisplayExcercises below.
        // DisplayExcercise1 Method

    }
}


