﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using MySwoleMate.BLL;
using MySwoleMate.Models;
using System.Configuration;
using System.Web.UI.WebControls;

namespace MySwoleMate
{
    public partial class ViewWorkout : System.Web.UI.Page
    {
        static string CS = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ToString();
        TraineeBLL traineebll = new TraineeBLL(CS);
        WorkoutBLL workoutBLL = new WorkoutBLL(CS);
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                // Bind Name
               
                var trainee = traineebll.GetTraineeById(Convert.ToInt32(Request.QueryString["TraineeID"]));

                var workout = workoutBLL.GetWorkoutById(trainee.WorkoutID);

                LblTraineeName.Text = string.Format("{0} {1}" , trainee.FirstName, trainee.LastName);
                LblWorkoutName.Text = string.Format("{0}",  workout.Name);
                LblWorkout1.Text = string.Format("{0}", workout.DisplayExcercise1);
                LblWorkout2.Text = string.Format("{0}", workout.DisplayExcercise2);
                LblWorkout3.Text = string.Format("{0}", workout.DisplayExcercise3);
                LblWorkout4.Text = string.Format("{0}", workout.DisplayExcercise4);
                LblWorkout5.Text = string.Format("{0}", workout.DisplayExcercise5);
                
               
                
            }

        }

        protected void WorkoutAssignButton_Click(object sender, EventArgs e)
        {
            string traineeID = Request.QueryString["TraineeID"].ToString();

            if (!string.IsNullOrWhiteSpace(traineeID))
            {
                Response.Redirect(string.Format("~/AssignWorkout.aspx?TraineeID={0}", traineeID));
            }
            else
            {
                Response.Redirect(string.Format("~/Trainees.aspx"));
            }
        }
    }
}