﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using MySwoleMate.BLL;
using MySwoleMate.Models;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace MySwoleMate
{
    public partial class AssignWorkout : System.Web.UI.Page
    {
        static string CS = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ConnectionString;
        TraineeBLL traineebll = new TraineeBLL(CS);
        WorkoutBLL workoutBLL = new WorkoutBLL(CS);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    var trainee = traineebll.GetTraineeById(Convert.ToInt32(Request.QueryString["TraineeID"]));
                    Label1.Text = string.Format("{0} {1}", trainee.FirstName, trainee.LastName);


                   // List<WorkoutViewModel> workoutList = workoutBLL.GetAllWorkouts();

                  
                    string Query = "Select * from Workout";
                    SqlDataAdapter adpt = new SqlDataAdapter(Query, con);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    con.Open();
                    
                    DropDownList1.DataTextField = "Name";
                    DropDownList1.DataValueField = "WorkoutID";
                    DropDownList1.DataSource = dt;

                    DropDownList1.DataBind();


                 
                    DropDownList1.Items.Insert(0, new ListItem("---Select Workout---", "0"));




                }
            }

        }

        protected void WorkoutAssignButton_Click(object sender, EventArgs e)
        {
            //Get Workout ID and Name ID
            if (Page.IsValid)
            {
                
                 if ((Convert.ToInt32(Request.QueryString["TraineeID"])) > 0 && Convert.ToInt16(DropDownList1.SelectedValue) > 0)
                {
                    traineebll.AssignWorkoutToTrainee(Convert.ToInt32(Request.QueryString["TraineeID"]), Convert.ToInt16(DropDownList1.SelectedValue));

                    Response.Redirect("~/Trainees.aspx");
                }
                else
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "Please Select Workout";
                }
            }
        }
    }
}
