﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="Workout.aspx.cs" Inherits="MySwoleMate.Workout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-10">
                    <h1>5-STEP WORKOUT LIST</h1>
                </div>
                <div class="col-xs-2">
                    <!--an anchor tag is used to provide a link to another resource (either in the same application or outside)-->
                    <!--The ~ symbol is used as a relative URL path to specify the resource is in the same application root-->
                    <a href="~/AddWorkout.aspx" runat="server" class="btn btn-success">Add New Workout</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">                  
                </div>
                <asp:GridView ID="WorkoutList" runat="server" CssClass="table table-bordered text-left"
                    AutoGenerateColumns="False" OnRowDeleting="WorkoutList_RowDeleting" DataKeyNames="WorkoutID" OnSelectedIndexChanged="WorkoutList_SelectedIndexChanged"
                    Width="1151px" Height="209px">
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="WorkoutID" DataNavigateUrlFormatString="~/EditWorkout.aspx?WorkoutID={0}" Text="Edit">
                        <ControlStyle CssClass="btn btn-success btn-xs" />
                        <ItemStyle CssClass="text-center" />
                        </asp:HyperLinkField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="DeleteButton" runat="server" Text="Delete" CommandName="Delete" 
                                    CssClass="btn btn-xs btn-default" OnClientClick="if(!confirm('Are you sure you wish to delete this trainee?')) return false;"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Workout" />
                        <asp:BoundField DataField="DisplayExcercise1" HeaderText="Step 1" />
                        <asp:BoundField DataField="DisplayExcercise2" HeaderText="Step 2" />
                        <asp:BoundField DataField="DisplayExcercise3" HeaderText="Step 3" />
                        <asp:BoundField DataField="DisplayExcercise4" HeaderText="Step 4" />
                        <asp:BoundField DataField="DisplayExcercise5" HeaderText="Step 5" />
                        <asp:BoundField />
                    </Columns>
                    
                </asp:GridView>
            </div>
        </div>
    </section>
</asp:Content>
