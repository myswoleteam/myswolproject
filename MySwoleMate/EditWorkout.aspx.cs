﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySwoleMate;
using System.Web.UI;
using MySwoleMate.BLL;
using MySwoleMate.Models;
using System.Configuration;
using System.Data.SqlClient;



using System.Web.UI.WebControls;

namespace MySwoleMate
{
    public partial class EditWorkout : System.Web.UI.Page
    {
        //Create a new instance of the business logic class for Workout, we created the instance
        //here so that we can use it with both the Page_Load and Click event for WorkoutEditButton
        WorkoutBLL workoutBLL = new WorkoutBLL(ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ToString());
        /// <summary>
        /// Method for Load event to load values into the form for edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            //Only load the values if the request is not a PostBack
            if (!IsPostBack)
            {
                // We pass the WorkoutID from the Gridview on Workout.aspx. See how we passed 
                //WorkoutID to this page from Workout.aspx by looking at HyperLinkField in the GridView control
                //on the Workout.aspx page for clarification

                WorkoutViewModel model = workoutBLL.GetWorkoutById(Convert.ToInt32(Request.QueryString["WorkoutID"]));
                //We use the GetWorkoutById method to get the Workout from the database, which we use to 
                //populate the data into the form.
                Name.Text = model.Name;
                Step1Exercise.Text = model.Exercise1;
                Step2Exercise.Text = model.Exercise2;
                Step3Exercise.Text = model.Exercise3;
                Step4Exercise.Text = model.Exercise4;
                Step5Exercise.Text = model.Exercise5;

                SetsStep1.Text = model.Exercise1Sets.ToString();
                RepsStep1.Text = model.Exercise1Reps.ToString();
                SetsStep2.Text = model.Exercise2Sets.ToString();
                RepsStep2.Text = model.Exercise2Reps.ToString();
                SetsStep3.Text = model.Exercise3Sets.ToString();
                RepsStep3.Text = model.Exercise3Reps.ToString();
                SetsStep4.Text = model.Exercise4Sets.ToString();
                RepsStep4.Text = model.Exercise4Reps.ToString();
                SetsStep5.Text = model.Exercise5Sets.ToString();
                RepsStep5.Text = model.Exercise5Reps.ToString();




            }
        }

        protected void EditWorkoutButton_Click(object sender, EventArgs e)
        {
            //Check to see if all ValidationControls are valid
            if (IsValid)
            {
                //Create new empty ViewModel to pass in the new values
                WorkoutViewModel workout = new WorkoutViewModel();
                //Use the same query string to fill the ID
                workout.WorkoutID = Convert.ToInt32(Request.QueryString["WorkoutID"]);
                workout.Exercise1 = Step1Exercise.Text;
                workout.Exercise2 = Step2Exercise.Text;
                workout.Exercise3 = Step3Exercise.Text;
                workout.Exercise4 = Step4Exercise.Text;
                workout.Exercise5 = Step5Exercise.Text;
                workout.Name = Name.Text;

                workout.Exercise1Sets = Convert.ToInt32(SetsStep1.Text);
                workout.Exercise1Reps = Convert.ToInt32(RepsStep1.Text);
                workout.Exercise2Sets = Convert.ToInt32(SetsStep2.Text);
                workout.Exercise2Reps = Convert.ToInt32(RepsStep2.Text);
                workout.Exercise3Sets = Convert.ToInt32(SetsStep3.Text);
                workout.Exercise3Reps = Convert.ToInt32(RepsStep3.Text);
                workout.Exercise4Sets = Convert.ToInt32(SetsStep4.Text);
                workout.Exercise4Reps = Convert.ToInt32(RepsStep4.Text);
                workout.Exercise5Sets = Convert.ToInt32(SetsStep5.Text);
                workout.Exercise5Reps = Convert.ToInt32(RepsStep5.Text);
                //Call the EditWorkout() method passing in the new values
                workoutBLL.EditWorkout(workout);
                //Redirect to the Workout.aspx page
                Response.Redirect("~/Workout.aspx");

            }
        }

        protected void Name_TextChanged(object sender, EventArgs e)
        {

        }
    }
}