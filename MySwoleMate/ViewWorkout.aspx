﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="ViewWorkout.aspx.cs" Inherits="MySwoleMate.ViewWorkout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>5-STEP WORKOUT</h1>
                    <h1>
                       <asp:Label ID="LblTraineeName" runat="server" Text="Text"></asp:Label>
                     </h1>
                </div>
            </div>
        
        <div class="row ">
            <br />
            <br />
                        <div class="col-xs-12 text-center">
                         
                         <h1>
                             <asp:Label ID="LblWorkoutName" runat="server" Text="Label"></asp:Label>

                         </h1>
                          <h3 id="ContentPlaceHolder1_DisplayExercise1">
                              <asp:Label ID="LblWorkout1" runat="server" Text="Label"></asp:Label>

                          </h3>                             
                          <h3>
                              <asp:Label ID="LblWorkout2" runat="server" Text="Label"></asp:Label>

                          </h3>                              
                          <h3>
                              <asp:Label ID="LblWorkout3" runat="server" Text="Label"></asp:Label>

                          </h3>                            
                          <h3> 
                              <asp:Label ID="LblWorkout4" runat="server" Text="Label"></asp:Label>

                          </h3>
                          <h3>
                              <asp:Label ID="LblWorkout5" runat="server" Text="Label"></asp:Label>

                          </h3>
                                 
                          <div class="col-xs-4 col-xs-offset-4 text-center">
                        <br />
                        <br />
                        <asp:Button ID="WorkoutAssignButton" runat="server" Text="Assign" CssClass="btn btn-success"
                            ValidationGroup="AllValidators" OnClick="WorkoutAssignButton_Click"  />
                        <a href="~/Trainees.aspx" runat="server" class="btn btn-default">Back</a>
                        </div>
                             
                        </div>
                    </div>
            </div>
    </section>
</asp:Content>
