﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="Scheduler.aspx.cs" Inherits="MySwoleMate.Progress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-10">
                    <h1>PROGRESS TRACKER</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="table-responsive">
                        <table class="table table-bordered text-left" id="TraineeList" style="border-collapse: collapse" border="1">
                            <tbody>
                                <tr>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">Progress Tracker</th>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <a class="btn btn-success btn-xs" href="EditTrainee.aspx?TraineeID= ">Christopher Chen</a>
                                    </td>
                                    <td>
                                        <div>
                                            <table class="table table-bordered text-left progress" style="border-collapse: collapse" border="1">
                                                <tbody>
                                                    <tr>
                                                        <th scope="col">&nbsp;</th>
                                                        <th scope="col">Date & Time of Appointment</th>
                                                        <th scope="col">Weight(lbs)</th>
                                                        <th scope="col">Waist(in)</th>
                                                        <th scope="col">Body Fat%</th>
                                                        <th scope="col">Chest(in)</th>
                                                        <th scope="col">Upper Arm (in)</th>
                                                    </tr>
                                                    <tr class="info">
                                                        <td class="text-center"><a class="btn btn-success btn-xs" href="EditMeasurment.aspx?">Edit</a></td>
                                                        <td class="value"><span id=""></span>100 </td>
                                                        <td class="value"><span id=""></span>890 </td>
                                                        <td class="value"><span id=""></span>660 </td>
                                                        <td class="value"><span id=""></span>500 </td>
                                                        <td class="value"><span id=""></span>580 </td>
                                                        <td class="value"><span id=""></span>450</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </section>
</asp:Content>
