﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="Scheduler.aspx.cs" Inherits="MySwoleMate.Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-10">
                    <h1>Scheduler</h1>
                </div>
                <div class="col-xs-2">
                    <a href="~/AddSchedule.aspx" runat="server" class="btn btn-success">Add New Appointment</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="table-responsive" >
                        <table class="table table-bordered text-left" style="border-collapse: collapse" border="1">
                            <tbody>
                                <tr>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">Appointment Date & Time</th>
                                    <th scope="col">Trainee Name</th>
                                    <th scope="col">Last Modified</th>
                                    <th scope="col">&nbsp;</th>
                                </tr>
                                <tr class="warning">
                                    <td class="text-center">
                                        <button class="btn btn-success btn-xs">Edit</button></td>
                                    <td class="text-center">
                                        <button class="btn btn-default btn-xs">Delete</button></td>
                                    <td>Date</td>
                                    <td>Name</td>
                                    <td>Last Modified</td>
                                    <td class="text-center">Enter Measurements</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>






        </div>

    </section>
</asp:Content>
