﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="EditWorkout.aspx.cs" Inherits="MySwoleMate.EditWorkout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1> EDIT WORKOUT</h1>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="NameLabel" runat="server" Text="Workout Name"
                        AssociatedControlID="Name" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox ID="Name" runat="server" CssClass="form-control" TextMode="SingleLine" MaxLength="30"
                            OnTextChanged="Name_TextChanged"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Workout Name is Required"
                            ControlToValidate="Name" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>

                </div>


                <!-- Step 1 -->
                <div class="form-group">
                    <asp:Label ID="Step1ExerciseLabel" runat="server" Text="Step 1-Exercise"
                        AssociatedControlID="Step1Exercise" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox ID="Step1Exercise" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Step 1-Exercise is Required"
                            ControlToValidate="Step1Exercise" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="SetsStep1Label" runat="server" Text="# of Sets" CssClass="col-xs-4 control-label"
                        AssociatedControlID="SetsStep1">
                    </asp:Label>

                    <div class="col-xs-1">
                        <asp:TextBox ID="SetsStep1" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                         <asp:RangeValidator ID="RangeValidatorSetsStep1" runat="server"
                            ErrorMessage="Sets Step must be between  1-199" 
                            ControlToValidate="SetsStep1" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredSetsStep1" runat="server" 
                            ErrorMessage="SetsStep1-Exercise is Required"
                            ControlToValidate="SetsStep1" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Label ID="RepsStep1Label" runat="server" Text="# of Reps"
                        AssociatedControlID="RepsStep1" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="RepsStep1" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidatorRepsStep1" runat="server"
                            ErrorMessage="Reps # must be between  1-199" 
                            ControlToValidate="RepsStep1" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredRepsStep1" runat="server" 
                            ErrorMessage="RepsStep1-Exercise is Required"
                            ControlToValidate="RepsStep1" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <!-- Step 2 -->
                <div class="form-group">
                    <asp:Label ID="Step2ExerciseLabel" runat="server" Text="Step 2-Exercise"
                        AssociatedControlID="Step2Exercise" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox ID="Step2Exercise" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Step 2-Exercise is Required"
                            ControlToValidate="Step1Exercise" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">

                    <asp:Label ID="SetsStep2Label" runat="server" Text="# of Sets" CssClass="col-xs-4 control-label"
                        AssociatedControlID="SetsStep2">
                    </asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="SetsStep2" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidatorSetsStep2" runat="server"
                            ErrorMessage="Sets Step must be between  1-199" 
                            ControlToValidate="SetsStep2" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredSetsStep2" runat="server" 
                            ErrorMessage="SetsStep2-Exercise is Required"
                            ControlToValidate="SetsStep2" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Label ID="RepsStep2Label" runat="server" Text="# of Reps"
                        AssociatedControlID="RepsStep2" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="RepsStep2" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidatorRepsStep2" runat="server"
                            ErrorMessage="Reps # must be between  1-199" 
                            ControlToValidate="RepsStep2" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredRepsStep2" runat="server" 
                            ErrorMessage="RepsStep2-Exercise is Required"
                            ControlToValidate="RepsStep2" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <!-- Step 3 -->
                <div class="form-group">
                    <asp:Label ID="Step3ExerciseLabel" runat="server" Text="Step 3-Exercise"
                        AssociatedControlID="Step3Exercise" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox ID="Step3Exercise" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Step 3-Exercise is Required"
                            ControlToValidate="Step1Exercise" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="SetsStep3Label" runat="server" Text="# of Sets" CssClass="col-xs-4 control-label"
                        AssociatedControlID="SetsStep3">
                    </asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="SetsStep3" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidatorSetsStep3" runat="server"
                            ErrorMessage="Sets Step must be between  1-199" 
                            ControlToValidate="SetsStep3" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredSetsStep3" runat="server" 
                            ErrorMessage="SetsStep3-Exercise is Required"
                            ControlToValidate="SetsStep3" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Label ID="RepsStep3Label" runat="server" Text="# of Reps"
                        AssociatedControlID="RepsStep3" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="RepsStep3" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                         <asp:RangeValidator ID="RangeValidatorRepsStep3" runat="server"
                            ErrorMessage="Reps # must be between  1-199" 
                            ControlToValidate="RepsStep3" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredRepsStep3" runat="server" 
                            ErrorMessage="RepsStep3-Exercise is Required"
                            ControlToValidate="RepsStep1" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                </div>

                <!-- Step 4 -->
                <div class="form-group">
                    <asp:Label ID="Step4ExerciseLabel" runat="server" Text="Step 4-Exercise"
                        AssociatedControlID="Step4Exercise" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox ID="Step4Exercise" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Step 4-Exercise is Required"
                            ControlToValidate="Step1Exercise" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">

                    <asp:Label ID="SetsStep4Label" runat="server" Text="# of Sets" CssClass="col-xs-4 control-label"
                        AssociatedControlID="SetsStep4">
                    </asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="SetsStep4" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px" ></asp:TextBox>
                         <asp:RangeValidator ID="RangeValidatorSetsStep4" runat="server"
                            ErrorMessage="Sets Step must be between  1-199" 
                            ControlToValidate="SetsStep4" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredSetsStep4" runat="server" 
                            ErrorMessage="SetsStep4-Exercise is Required"
                            ControlToValidate="SetsStep4" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Label ID="RepsStep4Label" runat="server" Text="# of Reps"
                        AssociatedControlID="RepsStep4" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="RepsStep4" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                        
                        <asp:RangeValidator ID="RangeValidatorRepsStep4" runat="server"
                            ErrorMessage="Reps # must be between  1-199" 
                            ControlToValidate="RepsStep4" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredRepsStep4" runat="server" 
                            ErrorMessage="RepsStep4-Exercise is Required"
                            ControlToValidate="RepsStep4" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                </div>

                <!-- Step 5 -->
                <div class="form-group">
                    <asp:Label ID="Step5ExerciseLabel" runat="server" Text="Step 5-Exercise"
                        AssociatedControlID="Step5Exercise" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox ID="Step5Exercise" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Step 5-Exercise is Required"
                            ControlToValidate="Step1Exercise" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">

                    <asp:Label ID="SetsStep5Label" runat="server" Text="# of Sets" CssClass="col-xs-4 control-label"
                        AssociatedControlID="SetsStep5">
                    </asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="SetsStep5" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                         <asp:RangeValidator ID="RangeValidatorSetsSteps5" runat="server"
                            ErrorMessage="Sets Step must be between  1-199" 
                            ControlToValidate="SetsStep5" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredSetsStep5" runat="server" 
                            ErrorMessage="SetsStep5-Exercise is Required"
                            ControlToValidate="SetsStep5" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Label ID="RepsStep5Label" runat="server" Text="# of Reps"
                        AssociatedControlID="RepsStep5" CssClass="col-xs-4 control-label"></asp:Label>
                    <div class="col-xs-1">
                        <asp:TextBox ID="RepsStep5" runat="server" CssClass="form-control" TextMode="SingleLine"
                            MaxLength="30" OnTextChanged="Name_TextChanged" Width="110px"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidatorRepsStep5" runat="server"
                            ErrorMessage="Reps # must be between  1-199" 
                            ControlToValidate="RepsStep1" ForeColor="Red" 
                            MinimumValue="1" MaximumValue="199" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredRepsSteps5" runat="server" 
                            ErrorMessage="RepsStep5-Exercise is Required"
                            ControlToValidate="SetsStep5" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <br /><br />

                <div class="form-group">
                    <div class="col-xs-4 col-xs-offset-4">
                        <asp:Button ID="EditWorkoutButton" runat="server" Text="Submit"
                            CssClass="btn btn-success" OnClick="EditWorkoutButton_Click" />
                        <asp:HyperLink CssClass="btn btn-default" NavigateUrl="~/Workout.aspx" runat="server" Text="Back" />
                    </div>
                </div>

            </div>

        </div>
    </section>

</asp:Content>
