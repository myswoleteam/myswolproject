﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MySwoleMate.Master" AutoEventWireup="true" CodeBehind="AssignWorkout.aspx.cs" Inherits="MySwoleMate.AssignWorkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="first">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>5-STEP WORKOUT LIST</h1>
                </div>
                <br />
                <div class="col-xs-12 text-center">
                    <h3>
                        <asp:Label ID="Label1" runat="server" Text="{No Trainee Selected}"></asp:Label>

                    </h3>
                </div>
                <br />
                <div class="form-group ">
                    <div class="col-xs-8 col-xs-offset-2 text-center">

                        <asp:DropDownList ID="DropDownList1" runat="server" Height="51px" Width="720px" CssClass="form-control">
                        </asp:DropDownList>
                        <br />
                         <asp:RequiredFieldValidator ID="WorkoutRequiredFieldValidator" runat="server" ErrorMessage="Please select anpther option other than --select option--" 
                        ControlToValidate="DropDownList1" InitialValue="0"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-4 col-xs-offset-4 text-center">
                        <br />
                        <br />
                        <asp:Button ID="WorkoutAssignButton" runat="server" Text="Assign" CssClass="btn btn-success"
                            ValidationGroup="AllValidators" OnClick="WorkoutAssignButton_Click" />
                        <a href="~/Trainees.aspx" runat="server" class="btn btn-default">Back</a>

                    </div>

                </div>

            </div>
        </div>
    </section>
</asp:Content>
