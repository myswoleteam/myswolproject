﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using MySwoleMate;
using MySwoleMate.BLL;
using MySwoleMate.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;


namespace MySwoleMate
{
    public partial class AddWorkout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Add Workout to DB from user input form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void AddWorkoutButton_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
            // Grab connection string from web.config
            string CS = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ConnectionString;
            // Create new instance of Dll
            WorkoutBLL bLL = new WorkoutBLL(CS);
            //Create new instance of ViewModel with data from the user
            WorkoutViewModel workout = new WorkoutViewModel();
            //The Text property of each control will contain the data from the user
            workout.Name = Name.Text;
            workout.Exercise1 = Step1Exercise.Text;
            workout.Exercise2 = Step2Exercise.Text;
            workout.Exercise3 = Step3Exercise.Text;
            workout.Exercise4 = Step4Exercise.Text;
            workout.Exercise5 = Step5Exercise.Text;
            //Since the Text property returns a string, some properties would need to be converted
            workout.Exercise1Sets = Convert.ToInt16(SetsStep1.Text); 
            workout.Exercise1Reps = Convert.ToInt16(RepsStep1.Text);
            workout.Exercise2Sets = Convert.ToInt16(SetsStep2.Text);
            workout.Exercise2Reps = Convert.ToInt16(RepsStep2.Text);
            workout.Exercise3Sets = Convert.ToInt16(SetsStep3.Text);
            workout.Exercise3Reps = Convert.ToInt16(RepsStep3.Text);
            workout.Exercise4Sets = Convert.ToInt16(SetsStep4.Text);
            workout.Exercise4Reps = Convert.ToInt16(RepsStep4.Text);
            workout.Exercise5Sets = Convert.ToInt16(SetsStep5.Text);
            workout.Exercise5Reps = Convert.ToInt16(RepsStep5.Text);

            // call business logic method to add user inputs
            bLL.AddWorkout(workout);
            //Return to the Trainees list after adding the trainee to database   
            Response.Redirect("~/Workout.aspx");

            }
            
            
               
            
        }

        protected void Name_TextChanged(object sender, EventArgs e)
        {

        }
    }
}