﻿using MySwoleMate.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MySwoleMate
{
    public partial class Workout : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MySwoleMateConnectionString"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               BindData();
            }
           
        }


        private void BindData()
        {
           WorkoutBLL workout = new WorkoutBLL(connectionString);
           WorkoutList.DataSource = workout.GetAllWorkouts();
           WorkoutList.DataBind();
        }

        protected void WorkoutList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int workOutID= Convert.ToInt32(WorkoutList.DataKeys[e.RowIndex].Value.ToString());
            WorkoutBLL bLL = new WorkoutBLL(connectionString);
            bLL.DeleteWorkout(workOutID);
            BindData();
        }

        
        protected void WorkoutList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }


}